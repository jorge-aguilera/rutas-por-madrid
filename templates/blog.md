---
title: "$starting.nombre"
date: ${today}
slug: ${starting.id}
thumbnailImagePosition: left
thumbnailImage: images/${starting.id}.jpg
metaAlignment: center
categories:
- ${starting.categoria}
tags:
${starting.etiquetas.collect{"- $it"}.join('\n')}

---

{{< alert warning >}}
Ruta de *${starting.recorrido}* metros (aprox)
{{< /alert >}}

<!--more-->

{{< image classes="clean center" src="/images/${starting.id}.jpg">}}

{{< blockquote >}}
${starting.descripcion}
{{< /blockquote >}}

{{< line_break >}}

{{< image classes="clean center" src="/images/mapa-${starting.id}.png">}}

{{< line_break >}}

[Ficha completa](/pdf/${starting.id}.pdf)

[Ver Mapa](/maps/ruta-${starting.id}.html)

1. $starting.nombre ${starting.direccion ? '('+starting.direccion+')' : ''}

<%list.eachWithIndex{ l, idx-> 
def url = currents.contains(l.id) ? "[$l.nombre](../${l.id}/index.html)" : l.nombre
out << 
"""
${idx+2}. $url ${l.direccion ? '('+l.direccion+')' : ''}, a $l.distancia metros
"""
}%>


## OpenData

La información mostrada en esta página procede de los datos abiertos del Ayuntamiento De Madrid

## Subscribete

Si deseas recibir una nueva ruta cada semana en tu correo [subscribete aquí](https://tinyletter.com/rutas-por-madrid)

También puedes unirte al canal de [Telegram](https://t.me/rutaspormadrid) 

