import groovy.xml.MarkupBuilder

starting = model.starting
baseURL = model.baseURL
list = model.list

writer = new StringWriter()
builder = new MarkupBuilder(writer)
builder.div {
    h2 "Nueva Ruta por Madrid disponible"
    p "Hola, qué tal va la semana?"
    p "Hoy te traemos una nueva ruta a explorar"
    br()
    img(src:"https://blog.rutas-por-madrid.es/images/${starting.id}.jpg")
    p"La propuesta esta vez es empezar la visita en"
    h4"$starting.nombre"
    cite "$starting.descripcion"
    br()
    p "Detalle de la ruta (aproximadamente $starting.recorrido metros):"
    h5 "Empezamos en $starting.direccion"
    p "Continúa con "
    ul {
        list.each {
            li "$it.nombre (a $it.distancia metros)"
        }
    }
    br()
    img(src:"https://blog.rutas-por-madrid.es/images/mapa-${starting.id}.png")
    br()
    p "Puedes consultar la ficha completa de la ruta en "
    a(href="$baseURL/${model.starting.id}/")
    br()
    p "Que la disfrutes"
}
html = writer.toString()
[
        "Á":"&Aacute;",
        "á":"&aacute;",
        "É":"&Eacute;",
        "é":"&eacute;",
        "Í":"&Iacute;",
        "í":"&iacute;",
        "Ñ":"&Ntilde;",
        "ñ":"&ntilde;",
        "Ó":"&Oacute;",
        "ó":"&oacute;",
        "Ú":"&Uacute;",
        "ú":"&uacute;",
        "Ü":"&Uuml;",
        "ü":"&uuml;",
        "«":"&laquo;",
        "»":"&raquo;",
        "¿":"&iquest;",
        "¡":"&iexcl;",
        "€":"&euro;",
        '\u0096':'-'
].each {
    html = html.replaceAll(it.key, it.value)
}
html