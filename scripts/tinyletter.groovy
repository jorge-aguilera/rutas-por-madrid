@Grab(group = 'javax.mail', module = 'mail', version = '1.4.7')

import javax.mail.*
import javax.mail.internet.*

String content = "build/${args[0]}/letter-${args[0]}.html"
String subject = args.length > 1 ? args[1] : 'Nueva RutaPorMadrid'

// Init constants of sender email account.
String user = System.getenv("TINYLETTER_USER")
String to = System.getenv("TINYLETTER_TO")

String password = System.getenv("TINYLETTER_PASSWORD")
String host = System.getenv("TINYLETTER_HOST")
String port = "465" // e.g. -> "465" "587"

// Set up properties.
Properties props = System.getProperties()
props.put("mail.debug", "true")
props.put("mail.smtp.user", user)
props.put("mail.smtp.host", host)
props.put("mail.smtp.port", port)
props.put("mail.smtp.starttls.enable", "false")
props.put("mail.smtp.ssl.protocols", "TLSv1.2");
props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory")
props.put("mail.smtp.auth", "true");
props.put("mail.smtp.ssl.trust", host)

// Set up message.
MimeMessage message = new MimeMessage(Session.getDefaultInstance(props))
message.setFrom(new InternetAddress(user))
message.addRecipients(Message.RecipientType.TO, new InternetAddress(to))
message.setSubject(subject)
message.setContent(new File(content).text, "text/html")

// Send message
Transport transport = message.session.getTransport("smtp");
transport.connect(host, user, password);
transport.sendMessage(message, message.getAllRecipients());
transport.close();
println message