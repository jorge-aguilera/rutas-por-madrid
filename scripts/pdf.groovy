import groovy.text.GStringTemplateEngine
import util.ParsePage
import util.LoadPojos
import util.Model

assert args.length > 0

pojos = new LoadPojos()
list = pojos.list(args[0])
starting = list[0]
parser = new ParsePage()
parser.parsePage(starting)

markers = ""
list.eachWithIndex { it,idx-> markers+="-m \"label:${idx+1}|${it.latitud},${it.longitud}\" " }

model = new Model()
model.imageFile(starting.id).bytes = "$starting.img".toURL().bytes
exec = """
./create-static-map --width 400 --height 400 -o ${model.thumbFile(starting.id)} $markers
"""
println exec
exec.execute().waitFor()


template = new GStringTemplateEngine().createTemplate(new File('templates/ficha.adoc'))
model.fichaFile(starting.id).text = template.make(model.model(list))

"npx asciidoctor-web-pdf --template-require ./templates/template.js build/${starting.id}/${starting.id}.adoc".execute()

