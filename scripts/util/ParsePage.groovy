package util

@GrabResolver(name="google",root="https://maven.google.com/")
@Grab('org.jsoup:jsoup:1.15.1')

import org.jsoup.*

def parsePage(element){
    def resp = Jsoup.connect(element.url).followRedirects(true).execute()
    doc = resp.parse()

    if (!element.descripcion) {
        desc = doc.select('div.tiny-text').first()?.text()
        element.descripcion = desc
    }

    element.img = doc.select('div.image-content.ic-right>img').first()?.absUrl("src")

    return element
}