import groovy.text.GStringTemplateEngine
import util.ParsePage
import util.LoadPojos
import util.Model

assert args.length > 0

pojos = new LoadPojos()
list = pojos.list(args[0])
starting = list[0]
parser = new ParsePage()
parser.parsePage(starting)

model = new Model()

template = new GStringTemplateEngine().createTemplate(new File('templates/telegram.template'))
model.telegramFile(starting.id).text = template.make(model.model(list))
